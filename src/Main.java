//Ryan Hartsock
//2-24-2014
//AI Project 1

import java.util.ArrayList;


public class Main {
	public static void main(String[] args) {
		
		//Build and populate the initial array
		int[] arr = new int[6];
		arr[0] = 3;
		arr[1] = 3;
		arr[2] = 1;
		arr[3] = 0;
		arr[4] = 0;
		arr[5] = 0;
		
		//Create an array list of nodes
		ArrayList<Node> states = new ArrayList<Node>();
		
		//Create an empty array
		int[] empty = new int[6];
		
		//Create a node n with the array arr from above
		Node n = new Node(arr);
		
		//Add n to the array list
		states.add(n);
		
		//Create a new conductor node with the empty array
		Node conductor = new Node(empty);
		
		//Set the conductor node to be n 
		conductor = n;
		conductor.parent = n;
		
		//Handle the + - switch from each level
		int depth = -1;
		
		int c = 0;
		while(c>=0){
			conductor = states.get(c);
			actionAll(conductor, depth);
			check(conductor, states);
			depth = depth*-1;
			
			//Check to see if the current state is a solution
			if(conductor.arr[3] == 3 && conductor.arr[4] == 3 && conductor.arr[5] == 1 )
			{
				System.out.println("< mWrong, cWrong, bWrong, mRight, cRight, bRight >");
				System.out.println("END");
				
				System.out.print("<");
				//Print the solution node
				for(int f = 0; f < 6; f++)
				{
					System.out.print(conductor.arr[f] + ",");
				}
				
				System.out.println(">");
				//Print the solution node parents/grandparents...
				while(conductor != n)
				{
					System.out.print("<");
					
					for(int f = 0; f < 6; f++)
					{
						System.out.print(conductor.parent.arr[f] + ",");
					}
					
					System.out.println(">");

					conductor = conductor.parent;
				}
				
				System.out.println("BEGIN");
				return;
			}
			c++; //Increase c by 1
		}
		
	}

	//Function which checks the children of a node to see if they are valid states 
	//and adds them to the array list if they are.
	private static void check(Node conductor, ArrayList<Node> states) {
		
		//Child 0
		//Check to make sure that there are at least as many or 0 missionaries as cannibals on the wrong side.
		//& make sure that there are at least as many or 0 missionaries as cannibals on the right side
		//& make sure that no values are less than 0 and that the boat is variable is either 0 or 1.
		if(((conductor.c0.arr[0] >= conductor.c0.arr[1]) || conductor.c0.arr[0] == 0 ) && 
				((conductor.c0.arr[3] >= conductor.c0.arr[4]) || conductor.c0.arr[3]==0)
				&& conductor.c0.arr[0]>=0 && conductor.c0.arr[1]>=0 && conductor.c0.arr[4]>=0 && conductor.c0.arr[3]>=0
				&& (conductor.c0.arr[2] == 0 || conductor.c0.arr[2] == 1)
				&& (conductor.c0.arr[5] == 0 || conductor.c0.arr[5] == 1))
		{
			int y = 1;
			//Check for a repeated state
			for( int i = 0; i < states.size(); i++)
			{
				if(states.get(i).getArray()[0]==conductor.c0.arr[0])
				{
					if(states.get(i).getArray()[1]==conductor.c0.arr[1])
					{
						if(states.get(i).getArray()[2]==conductor.c0.arr[2])
						{
							y = 0;
						}
					}
					
				}
			}
			//If the conditions are met and the state is not repeated, add it to the list
			if(y == 1)
			{
				states.add(conductor.c0);
			}
			
		}
		
		//Child 1
		//Check to make sure that there are at least as many or 0 missionaries as cannibals on the wrong side.
		//& make sure that there are at least as many or 0 missionaries as cannibals on the right side
		//& make sure that no values are less than 0 and that the boat is variable is either 0 or 1.
		if(((conductor.c1.arr[0] >= conductor.c1.arr[1]) || conductor.c1.arr[0] == 0 ) && 
				((conductor.c1.arr[3] >= conductor.c1.arr[4]) || conductor.c1.arr[3]==0)
				&& conductor.c1.arr[0]>=0 && conductor.c1.arr[1]>=0 && conductor.c1.arr[4]>=0 && conductor.c1.arr[3]>=0
				&& (conductor.c1.arr[2] == 0 || conductor.c1.arr[2] == 1)
				&& (conductor.c1.arr[5] == 0 || conductor.c1.arr[5] == 1))
		{
			int y = 1;
			//Check for a repeated state
			for( int i = 0; i < states.size(); i++)
			{
				if(states.get(i).getArray()[0]==conductor.c1.arr[0])
				{
					if(states.get(i).getArray()[1]==conductor.c1.arr[1])
					{
						if(states.get(i).getArray()[2]==conductor.c1.arr[2])
						{
							y = 0;
						}
					}
					
				}
			}
			//If the conditions are met and the state is not repeated, add it to the list
			if(y == 1)
			{
				states.add(conductor.c1);
			}
		}
		
		//Child 2
		//Check to make sure that there are at least as many or 0 missionaries as cannibals on the wrong side.
		//& make sure that there are at least as many or 0 missionaries as cannibals on the right side
		//& make sure that no values are less than 0 and that the boat is variable is either 0 or 1.		
		if(((conductor.c2.arr[0] >= conductor.c2.arr[1]) || conductor.c2.arr[0] == 0 ) && 
				((conductor.c2.arr[3] >= conductor.c2.arr[4]) || conductor.c2.arr[3]==0)
				&& conductor.c2.arr[0]>=0 && conductor.c2.arr[1]>=0 && conductor.c2.arr[4]>=0 && conductor.c2.arr[3]>=0
				&& (conductor.c2.arr[2] == 0 || conductor.c2.arr[2] == 1)
				&& (conductor.c2.arr[5] == 0 || conductor.c2.arr[5] == 1))
		{
			int y = 1;
			//Check for a repeated state
			for( int i = 0; i < states.size(); i++)
			{
				if(states.get(i).getArray()[0]==conductor.c2.arr[0])
				{
					if(states.get(i).getArray()[1]==conductor.c2.arr[1])
					{
						if(states.get(i).getArray()[2]==conductor.c2.arr[2])
						{
							y = 0;
						}
					}
					
				}
			}
			//If the conditions are met and the state is not repeated, add it to the list
			if(y == 1)
			{
				states.add(conductor.c2);
			}
		}
		
		//Child 3
		//Check to make sure that there are at least as many or 0 missionaries as cannibals on the wrong side.
		//& make sure that there are at least as many or 0 missionaries as cannibals on the right side
		//& make sure that no values are less than 0 and that the boat is variable is either 0 or 1.		
		if(((conductor.c3.arr[0] >= conductor.c3.arr[1]) || conductor.c3.arr[0] == 0 ) && 
				((conductor.c3.arr[3] >= conductor.c3.arr[4]) || conductor.c3.arr[3]==0)
				&& conductor.c3.arr[0]>=0 && conductor.c3.arr[1]>=0 && conductor.c3.arr[4]>=0 && conductor.c3.arr[3]>=0
				&& (conductor.c3.arr[2] == 0 || conductor.c3.arr[2] == 1)
				&& (conductor.c3.arr[5] == 0 || conductor.c3.arr[5] == 1))
		{
			int y = 1;
			//Check for a repeated state
			for( int i = 0; i < states.size(); i++)
			{
				if(states.get(i).getArray()[0]==conductor.c3.arr[0])
				{
					if(states.get(i).getArray()[1]==conductor.c3.arr[1])
					{
						if(states.get(i).getArray()[2]==conductor.c3.arr[2])
						{
							y = 0;
						}
					}
					
				}
			}
			//If the conditions are met and the state is not repeated, add it to the list
			if(y == 1)
			{
				states.add(conductor.c3);
			}
		}
		
		//Child 4
		//Check to make sure that there are at least as many or 0 missionaries as cannibals on the wrong side.
		//& make sure that there are at least as many or 0 missionaries as cannibals on the right side
		//& make sure that no values are less than 0 and that the boat is variable is either 0 or 1.
		if(((conductor.c4.arr[0] >= conductor.c4.arr[1]) || conductor.c4.arr[0] == 0 ) && 
				((conductor.c4.arr[3] >= conductor.c4.arr[4]) || conductor.c4.arr[3]==0)
				&& conductor.c4.arr[0]>=0 && conductor.c4.arr[1]>=0 && conductor.c4.arr[4]>=0 && conductor.c4.arr[3]>=0
				&& (conductor.c4.arr[2] == 0 || conductor.c4.arr[2] == 1)
				&& (conductor.c4.arr[5] == 0 || conductor.c4.arr[5] == 1))
		{
			int y = 1;
			//Check for a repeated state
			for( int i = 0; i < states.size(); i++)
			{
				if(states.get(i).getArray()[0]==conductor.c4.arr[0])
				{
					if(states.get(i).getArray()[1]==conductor.c4.arr[1])
					{
						if(states.get(i).getArray()[2]==conductor.c4.arr[2])
						{
							y = 0;
						}
					}
					
				}
			}
			//If the conditions are met and the state is not repeated, add it to the list
			if(y == 1)
			{
				states.add(conductor.c4);
			}
		}
	}

	private static void actionAll(Node conductor, int depth) {
	
		action1(conductor, depth);
		action2(conductor, depth);
		action3(conductor, depth);
		action4(conductor, depth);
		action5(conductor, depth);
		
	}
	
	private static void action1(Node n, int depth) {

			int s = depth;
			int[] array = new int[6];
			array[0] = n.arr[0] + s*1;
			array[1] = n.arr[1] + s*0;
			array[2] = n.arr[2] + s*1;
			array[3] = n.arr[3] + -s*1;
			array[4] = n.arr[4] + -s*0;
			array[5] = n.arr[5] + -s*1;
			n.c0 = new Node(array);
			n.c0.parent = n;
	}
	
	private static void action2(Node n, int depth) {
	
			int s = depth;
			int[] array = new int[6];
			array[0] = n.arr[0] + s*2;
			array[1] = n.arr[1] + s*0;
			array[2] = n.arr[2] + s*1;
			array[3] = n.arr[3] + -s*2;
			array[4] = n.arr[4] + -s*0;
			array[5] = n.arr[5] + -s*1;
			n.c1 = new Node(array);	
			n.c1.parent = n;
	}
	
	private static void action3(Node n, int depth) {

			int s = depth;
			int[] array = new int[6];
			array[0] = n.arr[0] + s*0;
			array[1] = n.arr[1] + s*1;
			array[2] = n.arr[2] + s*1;
			array[3] = n.arr[3] + -s*0;
			array[4] = n.arr[4] + -s*1;
			array[5] = n.arr[5] + -s*1;
			n.c2 = new Node(array);
			n.c2.parent = n;
	}
	
	private static void action4(Node n, int depth) {
		
			int s = depth;
			int[] array = new int[6];
			array[0] = n.arr[0] + s*0;
			array[1] = n.arr[1] + s*2;
			array[2] = n.arr[2] + s*1;
			array[3] = n.arr[3] + -s*0;
			array[4] = n.arr[4] + -s*2;
			array[5] = n.arr[5] + -s*1;
			n.c3 = new Node(array);	
			n.c3.parent = n;
	}
	
	private static void action5(Node n, int depth) {

			int s = depth;
			int[] array = new int[6];
			array[0] = n.arr[0] + s*1;
			array[1] = n.arr[1] + s*1;
			array[2] = n.arr[2] + s*1;
			array[3] = n.arr[3] + -s*1;
			array[4] = n.arr[4] + -s*1;
			array[5] = n.arr[5] + -s*1;
			n.c4 = new Node(array);
			n.c4.parent = n;
	}
}
